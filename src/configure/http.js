import {fetch} from './axios'

export default {
    login: function (params) {
        return fetch('admin/page/login', params)
    },
    wxConfig: function (params) {
        return fetch('admin/wx/config', params)
    },
    editWxConfig: function (params) {
        return fetch('admin/wx/editConfig', params)
    },
    getGoodsCategory: function (params) {
        return fetch('admin/goods/category', params)
    },
    getGoodsBrand: function (params) {
        return fetch('admin/goods/brand', params)
    },
    getGoodsAttributeGroup: function (params) {
        return fetch('admin/goods/attributeGroup', params)
    },
    getGoodsAttributeKey: function (params) {
        return fetch('admin/goods/attributeKey', params)
    },
    getGoodsAttributeValue: function (params) {
        return fetch('admin/goods/attributeValue', params)
    },
    addGoods: function (params) {
        return fetch('admin/goods/add', params)
    }
}
