import axios from 'axios'
import qs from 'qs'
import conf from './index'

axios.defaults.timeout = 100000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'
axios.defaults.baseURL = conf.api
// POST传参序列化(添加请求拦截器)
axios.interceptors.request.use((config) => {
    // 判断是否存在token，如果存在的话，则每个http header都加上token
    if (localStorage.getItem('token')) {
        config.headers.Token = localStorage.getItem('token')
    }
    // 在发送请求之前做某件事
    if (config.method === 'post') {
        config.data = qs.stringify(config.data)
    }
    return config
}, (error) => {
    return Promise.reject(error)
})
// 返回状态判断(添加响应拦截器)
axios.interceptors.response.use((res) => {
    // 判断请求头是否有token，如果有则刷新本地token
    if (res.headers.Token) {
        localStorage.setItem('token', res.headers.Token)
    }
    // 对响应数据做些事
    return Promise.resolve(res.data)
}, (error) => {
    return Promise.reject(error)
})

// 返回一个Promise(发送post请求)
export function fetch (url, params) {
    return new Promise((resolve, reject) => {
        axios.post(url, params)
            .then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
            .catch((error) => {
                reject(error)
            })
    })
}
