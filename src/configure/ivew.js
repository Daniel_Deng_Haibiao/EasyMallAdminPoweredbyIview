import Vue from 'vue'
import {
    Menu,
    MenuItem,
    Icon,
    Row,
    Col,
    Breadcrumb,
    BreadcrumbItem,
    Button,
    ButtonGroup,
    Form,
    Select,
    Option,
    Input,
    RadioGroup,
    Radio,
    CheckboxGroup,
    Checkbox,
    Avatar,
    Message,
    Notice,
    Table,
    Steps,
    Step,
    Cascader,
    Tag
} from 'iview'

Vue.prototype.$Message = Message
Vue.prototype.$Notice = Notice

Vue.component('Submenu', Menu.Sub)
Vue.component('MenuGroup', Menu.Group)
Vue.component('Menu', Menu)
Vue.component('MenuItem', MenuItem)
Vue.component('Icon', Icon)
Vue.component('Row', Row)
Vue.component('Col', Col)
Vue.component('Breadcrumb', Breadcrumb)
Vue.component('BreadcrumbItem', BreadcrumbItem)
Vue.component('Button', Button)
Vue.component('ButtonGroup', ButtonGroup)
Vue.component('Form', Form)
Vue.component('FormItem', Form.Item)
Vue.component('Select', Select)
Vue.component('Option', Option)
Vue.component('Input', Input)
Vue.component('RadioGroup', RadioGroup)
Vue.component('Radio', Radio)
Vue.component('CheckboxGroup', CheckboxGroup)
Vue.component('Checkbox', Checkbox)
Vue.component('Avatar', Avatar)
Vue.component('Table', Table)
Vue.component('Steps', Steps)
Vue.component('Step', Step)
Vue.component('Cascader', Cascader)
Vue.component('Tag', Tag)
