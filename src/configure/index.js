export default {
    api: process.env.NODE_ENV === 'development' ? 'http://localhost:8010/' : 'http://api.neho.top/'
}
