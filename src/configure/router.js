import Vue from 'vue'
import VueRouter from 'vue-router'
import utils from '../utils'

Vue.use(VueRouter)
let routes = [
    {
        name: '/',
        path: '/',
        redirect: {
            path: '/index'
        }
    },
    {
        name: 'public-layout',
        path: '/public-layout',
        component: (resolve) => {
            require(['../layout/public-layout'], resolve)
        },
        children: [
            {
                name: 'login',
                path: '/login',
                component: (resolve) => {
                    require(['../view/login'], resolve)
                }
            }
        ]
    }, {
        name: 'auth-layout',
        path: '/auth-layoutt',
        meta: {
            requireLogin: true
        },
        component: (resolve) => {
            require(['../layout/auth-layout'], resolve)
        },
        children: [
            {
                name: 'auth-index-layout',
                path: '/auth-index-layout',
                component: (resolve) => {
                    require(['../layout/auth-index-layout'], resolve)
                },
                children: [
                    {
                        name: 'index',
                        path: '/index',
                        component: (resolve) => {
                            require(['../view/index/index'], resolve)
                        }
                    }
                ]
            }, {
                name: 'auth-wx-layout',
                path: '/auth-wx-layout',
                component: (resolve) => {
                    require(['../layout/auth-wx-layout'], resolve)
                },
                children: [
                    {
                        name: 'wx',
                        path: '/wx',
                        component: (resolve) => {
                            require(['../view/wx/index'], resolve)
                        }
                    },
                    {
                        name: 'wx-config',
                        path: '/wx/config',
                        component: (resolve) => {
                            require(['../view/wx/config'], resolve)
                        }
                    },
                    {
                        name: 'wx-config-edit',
                        path: '/wx/config/edit',
                        component: (resolve) => {
                            require(['../view/wx/config-edit.vue'], resolve)
                        }
                    }
                ]
            },
            {
                name: 'auth-mall-layout',
                path: '/auth-mall-layout',
                component: (resolve) => {
                    require(['../layout/auth-mall-layout'], resolve)
                },
                children: [
                    {
                        name: 'mall',
                        path: '/mall',
                        component: (resolve) => {
                            require(['../view/mall/index'], resolve)
                        }
                    },
                    {
                        name: 'mall-goods',
                        path: '/mall/goods',
                        component: (resolve) => {
                            require(['../view/mall/goods/index'], resolve)
                        }
                    },
                    {
                        name: 'mall-goods-add',
                        path: '/mall/goods/add',
                        component: (resolve) => {
                            require(['../view/mall/goods/add'], resolve)
                        }
                    }
                ]
            },
            {
                name: 'auth-setting-layout',
                path: '/auth-setting-layout',
                component: (resolve) => {
                    require(['../layout/auth-setting-layout'], resolve)
                },
                children: [
                    {
                        name: 'setting',
                        path: '/setting',
                        component: (resolve) => {
                            require(['../view/setting/index'], resolve)
                        }
                    }
                ]
            }
        ]
    }]
export const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    /*
    * 用户登录状态判断
    * */
    if (to.matched.some(record => record.meta.requireLogin)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!utils.isLogin()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        if (utils.isLogin() && to.name === 'login') {
            next({
                path: '/index',
                query: { redirect: to.fullPath }
            })
        }
        next() // 确保一定要调用 next()
    }
})
