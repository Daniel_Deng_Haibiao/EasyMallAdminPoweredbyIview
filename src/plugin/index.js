import Vue from 'vue'
import http from '../configure/http'
import methods from './index-methods'

function install (Vue) {
    Vue.prototype.$http = http
    Vue.mixin({
        methods: methods
    })
}

Vue.use(install)
