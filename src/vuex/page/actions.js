import * as types from './mutation-types'

export default {
    addGoodsCategory({commit}, n) {
        commit(types.ADD_GOODS_CATEGORY, n);
    },
    addGoodsBase({commit}, o) {
        commit(types.ADD_GOODS_BASE, o);
    },
    addGoodsAttribute({commit}, o) {
        commit(types.ADD_GOODS_ATTRIBUTE, o);
    },
}