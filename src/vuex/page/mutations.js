import * as types from './mutation-types'

export default {
    [types.ADD_GOODS_CATEGORY](state, n) {
        state.addGoods.category = n;
    },
    [types.ADD_GOODS_BASE](state, o) {
        state.addGoods.brand = o.brand;
        state.addGoods.sn = o.sn;
        state.addGoods.name = o.name;
        state.addGoods.marketPrice = o.price;
        state.addGoods.stock = o.stock;
        state.addGoods.primaryPic = o.img;
    },
    [types.ADD_GOODS_ATTRIBUTE](state, o) {
        state.addGoods.attributeKey = o.key;
        state.addGoods.attributeValue = o.value;
    }
}