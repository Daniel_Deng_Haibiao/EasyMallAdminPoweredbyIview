
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import pageModule from './page/module'


export default new Vuex.Store({
    strict: true,
    modules: {
        page: pageModule
    }
})
