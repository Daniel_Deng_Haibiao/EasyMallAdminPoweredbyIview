// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import {router} from './configure/router'
import './assets/iconfont/index.css'
import './assets/less/Normalize.less'
import 'iview/dist/styles/iview.css'
import './configure/ivew'
import './components'
import './plugin'
import store from './vuex'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {App}
})
