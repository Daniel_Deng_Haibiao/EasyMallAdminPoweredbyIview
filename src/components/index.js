import Vue from 'vue'
import example from './example'
import divider from './divider'
import header from './header'
import imgUploader from './img-uploader'

Vue.component(example.name, example)
Vue.component(divider.name, divider)
Vue.component(header.name, header)
Vue.component(imgUploader.name, imgUploader)
